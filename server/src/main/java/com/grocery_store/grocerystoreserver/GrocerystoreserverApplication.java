package com.grocery_store.grocerystoreserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrocerystoreserverApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(GrocerystoreserverApplication.class, args);
    }

}
