package com.grocery_store.grocerystoreserver.controller;

import com.grocery_store.grocerystoreserver.model.StoreItemsModel;
import com.grocery_store.grocerystoreserver.repository.StoreItemsRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class StoreItemsController
{
    // ***** Member variables *****

    private final StoreItemsRepository item_repo_;

    // ***** Constructors *****

    public StoreItemsController(StoreItemsRepository item_repo) { item_repo_ = item_repo; }

    // ***** Functions *****

    @GetMapping("/items")
    public List<StoreItemsModel> getItems()
    {
        return (List<StoreItemsModel>) item_repo_.findAll();
    }

}
