package com.grocery_store.grocerystoreserver.repository;

import com.grocery_store.grocerystoreserver.model.StoreItemsModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreItemsRepository extends CrudRepository<StoreItemsModel, Long>
{

}
